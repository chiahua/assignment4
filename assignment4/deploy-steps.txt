echo "This is the deploy step"

export PROJECT_ID=spr2019cloudcicd-chiahua
gcloud config set project $PROJECT_ID
export CLOUDSDK_COMPUTE_ZONE=us-central1-b

gcloud container clusters get-credentials cicdcluster-1

kubectl delete deployment movies-deployment || echo "movies-deployment deployment does not exist"
kubectl delete service movies-deployment || echo "movies-deployment service does not exist"
kubectl delete ingress movies-ingress || echo "movies-ingress does not exist"

kubectl create -f deployment.yaml
kubectl expose deployment movies-deployment --target-port=5000 --type=NodePort

kubectl apply -f ingress.yaml

echo "Done deploying"
