#!/usr/bin/env python3

import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


app = Flask(__name__)
# app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)

    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies (id int NOT NULL AUTO_INCREMENT, year int NOT NULL, title varchar(255) NOT NULL, director varchar(255) NOT NULL, actor varchar(255) NOT NULL, date varchar(11) NOT NULL, rating int NOT NULL, PRIMARY KEY(id));'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    #cur.execute("INSERT INTO message (greeting) values ('Hello, World!')")
    #cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    #cur.execute("SELECT greeting FROM message")
    #entries = [dict(greeting=row[0]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


# @app.route('/add_to_db', methods=['POST'])
# def add_to_db():
#     print("Received request.")
#     print(request.form['message'])
#     msg = request.form['message']

#     db, username, password, hostname = get_db_creds()

#     cnx = ''
#     try:
#         cnx = mysql.connector.connect(user=username, password=password,
#                                       host=hostname,
#                                       database=db)
#     except Exception as exp:
#         print(exp)
#         import MySQLdb
#         cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

#     cur = cnx.cursor()
#     cur.execute("INSERT INTO message (greeting) values ('" + msg + "')")
#     cnx.commit()
#     return hello()

@app.route('/add_to_db', methods=['POST'])
def addMovieToDB():
    print("Received request.")
    year = request.form['year']
    title = "'" + request.form['title'] + "'"
    director = "'" + request.form['director'] + "'"
    actor = "'" + request.form['actor'] + "'"
    day = "'" + request.form['date'] + "'"
    rating = request.form['rating']
    error = ""
    if len(title)==2:
        error = "Movie " + title + " could not be inserted - Title field required"
    if len(director)==2:
        error = "Movie " + title + " could not be inserted - Invalid Director"
    if len(actor)==2:
        error = "Movie " + title + " could not be inserted - Invalid Actor"
    if len(day)==2:
        error = "Movie " + title + " could not be inserted - Invalid Date"
    if '-' in rating:
        error = "Movie " + title + " could not be inserted - Invalid Rating"
    if '-' in year:
        error = "Movie " + title + " could not be inserted - Invalid Year"
    if len(error)!=0:
        return render_template('index.html', addStatus=error)

    db, username, password, hostname = get_db_creds()
 
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)  
    result = ""
    try:
        cur.execute("SELECT COUNT(title) FROM movies WHERE UPPER(" + title + ")=UPPER(title)")
        matches = cur.fetchone()[0]
        if matches > 0:
            cur.execute("UPDATE movies SET year="+ year + ", director=" + director + ", actor=" + actor + ", date=" + day + ", rating=" + rating+ " WHERE title="+title)  
            cnx.commit()
            result = "Movie " + title + " successfully updated"
        else:    
            cur.execute("INSERT INTO movies (year, title, director, actor, date, rating) values ("+year+","+title+","+director+","+actor+","+day+","+rating+")")
            cnx.commit()
            result = "Movie " + title + " successfully inserted"
    except Exception as exp:
        result = "Movie " + title + " could not be inserted - " + str(exp)

    return render_template('index.html', addStatus=result)

@app.route('/delete_from_db', methods=['POST'])
def deleteMovieFromDB():
    print("Received request.")
    title = "'" + request.form['title'] + "'"
    error = ""
    if len(title)==2:
        error = "Movie " + title + " could not be deleted - Invalid Actor"
    if len(error)!=0:
        return render_template('index.html',deleteStatus=error)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)  
    result = ""
    try:
        cur.execute("SELECT COUNT(title) FROM movies WHERE UPPER(" + title + ")=UPPER(title)")
        cnx.commit()
        matches = cur.fetchone()[0]
        if matches > 0:
            cur.execute("DELETE FROM movies WHERE UPPER(" + title + ")=UPPER(title)")  
            cnx.commit()
            result = "Movie " + title + " successfully deleted"
        else:    
            result = "Movie with " + title + " does not exist"
    except Exception as exp:
        result = "Movie " + title + " could not be deleted - " + str(exp)

    return render_template('index.html', deleteStatus=result)

@app.route('/search_from_db', methods=['GET'])
def searchDB():
    print("Received request.")
    actor = str(request.args.get('actor'))
    
    if len(actor)==0:
        error = "Error - Actor is required field"
        return render_template('index.html', searchError=error)

    db, username, password, hostname = get_db_creds()
 
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()  
    result = []
    try:    
        cur.execute("SELECT title, year, actor FROM movies WHERE actor LIKE '%" + actor+"%'")
        results = cur.fetchall()
        if len(results) == 0:
            return render_template('index.html', searchError="No movies found for actor '" + actor + "'")
        else:
            result.append(('Title', 'Year', 'Actor'))
            for row in results:
                result.append((str(row[0]), str(row[1]), str(row[2])))
            return render_template('index.html', searchResults=result)
    except Exception as exp:
        error = "Error " + str(exp)
        return render_template('index.html', searchError=error)

@app.route('/search_from_highest', methods=['GET'])
def highestRated():
    print("Received request.")

    db, username, password, hostname = get_db_creds()
 
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()  
    result = ""
    try:
        cur.execute("SELECT rating FROM movies ORDER BY rating DESC LIMIT 1")
        targetRating = str(cur.fetchone()[0])
        cur.execute("SELECT title, year, actor, director, rating FROM movies WHERE rating="+targetRating)
        result = []
        result.append(('Title', 'Year', 'Actor', 'Director', 'Rating'))
        results = cur.fetchall()
        if len(results) == 0:
            return render_template('index.html', ratingError="No Movies in Database")
        else:
            for row in results:
                result.append((str(row[0]), str(row[1]), str(row[2]), str(row[3]),str(row[4])))
    except Exception as exp:
        return render_template('index.html', ratingError="Error " + str(exp))

    return render_template('index.html', ratingResults=result)

@app.route('/search_from_lowest', methods=['GET'])
def lowestRated():
    print("Received request.")

    db, username, password, hostname = get_db_creds()
 
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()  
    result = ""
    try:
        cur.execute("SELECT rating FROM movies ORDER BY rating ASC LIMIT 1")
        targetRating = str(cur.fetchone()[0])
        cur.execute("SELECT title, year, actor, director, rating FROM movies WHERE rating="+targetRating)
        result = []
        result.append(('Title', 'Year', 'Actor', 'Director', 'Rating'))
        results = cur.fetchall()
        if len(results) == 0:
            return render_template('index.html', ratingError="No Movies in Database")
        else:
            for row in results:
                result.append((str(row[0]), str(row[1]), str(row[2]), str(row[3]),str(row[4])))
    except Exception as exp:
        return render_template('index.html', ratingError="Error " + str(exp))

    return render_template('index.html', ratingResults=result)


@app.route("/")
def hello():
    return render_template('index.html')


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
