FROM ubuntu:14.04
RUN apt-get update -y \ 
    && apt-get install -y python3-setuptools 
RUN apt-get install -y python3-pip 
RUN apt-get install -y python-mysqldb
ADD requirements.txt /src/requirements.txt
RUN cd /src; pip3 install -r requirements.txt
ADD . /src
EXPOSE 5000
CMD ["python3", "./src/movies_web_app.py"]
